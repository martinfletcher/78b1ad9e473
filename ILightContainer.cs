﻿namespace LightBoxChallenge
{
    using LightBoxChallenge.Domain;

    using System;
    using System.Collections.Generic;

    public interface ILightContainer
    {
        IEnumerable<LightBase> Lights { get; }
    }
}
