﻿namespace LightBoxChallenge.Domain
{
    using System;

    public class EmergencyLight : LightBase
    {
        public EmergencyLight(String id, ConsoleColor colour) : base(id, colour, LightStatus.On)
        { }

        public override void SwitchOff()
        {
            Console.WriteLine("Can not turn off an Emergency Light");
        }
    }
}
