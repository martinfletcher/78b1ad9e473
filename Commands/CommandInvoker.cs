﻿namespace LightBoxChallenge.Commands
{
    using System;

    public abstract class CommandInvoker : ICommandInvoker
    {
        protected ICommand Command { get; private set; }

        protected virtual void SetCommand(ICommand command)
        {
            if (command == null) throw new ArgumentNullException("command");
            
            this.Command = command;
        }

        protected virtual void Invoke()
        {
            this.Command.Execute();
        }

        void ICommandInvoker.SetCommand(ICommand command)
        {
            this.SetCommand(command);
        }
        void ICommandInvoker.Invoke()
        {
            this.Invoke();
        }
    }
}
