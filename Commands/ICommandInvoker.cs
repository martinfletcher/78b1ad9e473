﻿namespace LightBoxChallenge.Commands
{
    using System;

    public interface ICommandInvoker
    {
        void SetCommand(ICommand command);

        void Invoke();
    }
}
