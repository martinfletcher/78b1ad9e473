﻿namespace LightBoxChallenge.Controllers
{
    using LightBoxChallenge.Domain;

    using System;
    using System.Collections.Generic;

    public interface ILightBoxController : ILightContainer
    {
        Int32 Capacity { get; }

        void AddLight(LightBase light);

        void SwitchLightOnByID(String id);
        void SwitchLightOffByID(String id);
        void SwitchOnAllLights();
        void SwitchOffAllLights();
        void ToggleLights();
    }
}
