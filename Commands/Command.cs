﻿namespace LightBoxChallenge.Commands
{
    using System;

    public abstract class Command : ICommand
    {
        protected abstract void Execute();
        protected abstract void Undo();

        void ICommand.Execute()
        {
            this.Execute();
        }
        void ICommand.Undo()
        {
            this.Undo();
        }
    }
}
