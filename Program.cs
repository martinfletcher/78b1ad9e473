﻿namespace LightBoxChallenge
{
    using LightBoxChallenge.Commands;
    using LightBoxChallenge.Controllers;
    using LightBoxChallenge.Domain;

    using System;
    using System.Collections.Generic;
    using System.Linq;

    class Program
    {
        static ILightBoxController Controller { get; set; }
        static IList<LightBase> StandardLights { get; set; }
        static IList<LightBase> EmergencyLights { get; set; }

        static void Main(string[] args)
        {
            Controller = new MaxTenLightBoxController(10, new DefaultCommandInvoker());

            StandardLights = new LightBase[]
            {
                new StandardLight("StandardLightRed", ConsoleColor.Red),
                new StandardLight("StandardLightGreen", ConsoleColor.Green),
                new StandardLight("StandardLightBlue", ConsoleColor.Blue),
                new StandardLight("StandardLightCyan", ConsoleColor.Cyan),
                new StandardLight("StandardLightMagenta", ConsoleColor.Magenta)
            };

            EmergencyLights = new LightBase[]
            {
                new EmergencyLight("EmergencyLightYellow", ConsoleColor.Yellow)
            };

            OutputAllLightsToConsole();
            Console.WriteLine("Press <Enter> to proceed");
            Console.ReadLine();

            AddLightsToController();
            Console.WriteLine("Press <Enter> to proceed");
            Console.ReadLine();

            TurnOnLightsByID();
            Console.WriteLine("Press <Enter> to proceed");
            Console.ReadLine();

            OutputAllLightsToConsole();
            Console.WriteLine("Press <Enter> to proceed");
            Console.ReadLine();

            AddLightToController(StandardLights[0]);
            Console.WriteLine("Press <Enter> to proceed");
            Console.ReadLine();

            ToggleLights();
            Console.WriteLine("Press <Enter> to proceed");
            Console.ReadLine();

            OutputAllLightsToConsole();
            Console.WriteLine("Press <Enter> to proceed");
            Console.ReadLine();

            SwitchAllLightsOn();
            Console.WriteLine("Press <Enter> to proceed");
            Console.ReadLine();

            OutputAllLightsToConsole();
            Console.WriteLine("Press <Enter> to proceed");
            Console.ReadLine();

            DisplayLightStatusCount(LightStatus.On);
            Console.WriteLine("Press <Enter> to proceed");
            Console.ReadLine();

            AddLightToController(EmergencyLights[0]);
            Console.WriteLine("Press <Enter> to proceed");
            Console.ReadLine();

            TurnOnLightByID(EmergencyLights[0]);
            Console.WriteLine("Press <Enter> to proceed");
            Console.ReadLine();

            OutputAllLightsToConsole();
            Console.WriteLine("Press <Enter> to proceed");
            Console.ReadLine();

            TurnOffLightByID(EmergencyLights[0]);
            Console.WriteLine("Press <Enter> to proceed");
            Console.ReadLine();

            OutputAllLightsToConsole();
            Console.WriteLine("Press <Enter> to proceed");
            Console.ReadLine();

            DisplayPowerUsage();
            Console.WriteLine();
            
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("Press <ENTER> to exit.");
            Console.ReadLine();
        }

        static void OutputAllLightsToConsole()
        {
            if (!Controller.Lights.Any())
            {
                Console.WriteLine("There are no light currently in controller");
                return;
            }

            foreach (LightBase light in Controller.Lights)
            {
                Console.ForegroundColor = light.Colour;
                Console.WriteLine(light.ToString());
            }

            Console.ForegroundColor = ConsoleColor.White;
        }

        static void AddLightsToController()
        {
            foreach(LightBase light in StandardLights)
            {
                AddLightToController(light);
            }                
        }        
        static void AddLightToController(LightBase light)
        {
            try
            {
                Console.ForegroundColor = light.Colour;

                Controller.AddLight(light);

                Console.ForegroundColor = ConsoleColor.White;
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                Console.ForegroundColor = ConsoleColor.White;
            }            
        }

        static void TurnOnLightsByID()
        {
            TurnOnLightByID(StandardLights[1]);
            TurnOnLightByID(StandardLights[4]);
        }
        static void TurnOnLightByID(LightBase light)
        {
            try
            {
                Controller.SwitchLightOnByID(light.ID);
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
            }
        }

        static void TurnOffLightByID(LightBase light)
        {
            try
            {
                Console.ForegroundColor = light.Colour;

                Controller.SwitchLightOffByID(light.ID);

                Console.ForegroundColor = ConsoleColor.White;
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
            }
        }

        static void ToggleLights()
        {
            try
            {
                Controller.ToggleLights();
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
            }
        }

        static void SwitchAllLightsOn()
        {
            try
            { 
                Controller.SwitchOnAllLights();
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
            }
        }

        static void DisplayLightStatusCount(LightStatus status)
        {
            Console.WriteLine(
                "Total number of {0} lights is {1}",
                status,
                Controller.Lights.Count(l => l.Status == status)
            );
        }

        static void DisplayPowerUsage()
        {
            PowerMonitor powerMonitor = new PowerMonitor(Controller);

            powerMonitor.ShowPowerUsage();
        }
    }
}
