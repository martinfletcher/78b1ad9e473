﻿namespace LightBoxChallenge
{
    using LightBoxChallenge.Domain;

    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class PowerMonitor
    {
        /// <summary>
        /// The PowerMonitor class takes in any brand of light box controller or any device that is capable of reporting the number of lights it has.
        /// 
        /// When the ShowPowerUsage method is called, it displays the power usage which is the number of lights multiplied by 100.
        /// 
        /// The task:
        /// 
        /// - Fix the class to be compatible with your light box controller
        /// - Modify the PowerMonitor class to be able to take in *any* device that has lights on it. You may need to alter your LightBoxController class
        ///   and/or create another class to do this.
        /// 
        /// </summary>

        private ILightContainer _lightContainer;

        protected IEnumerable<LightBase> Lights
        {
            get
            {
                return this._lightContainer.Lights;
            }
        }

        public PowerMonitor(ILightContainer lightContainer)
        {
            if (lightContainer == null) throw new ArgumentNullException("lightContainer");

            this._lightContainer = lightContainer;
        }

        public void ShowPowerUsage()
        {
            Console.WriteLine(
                "The power usage is: {0}w",
                this.Lights.Count(l => l.Status == LightStatus.On) * 100
            );
        }
    }
}
