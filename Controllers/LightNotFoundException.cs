﻿namespace LightBoxChallenge.Controllers
{
    using System;
    using System.Runtime.Serialization;

    [Serializable]
    public class LightNotFoundException : Exception
    {
        public LightNotFoundException(String message) : base(message)
        { }
        public LightNotFoundException(String message, Exception innerException) : base(message, innerException)
        { }

        protected LightNotFoundException(SerializationInfo info, StreamingContext context) : base(info, context)
        { }
    }
}
