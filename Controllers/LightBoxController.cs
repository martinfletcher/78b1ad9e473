﻿namespace LightBoxChallenge.Controllers
{
    using LightBoxChallenge.Commands;
    using LightBoxChallenge.Domain;

    using System;
    using System.Collections.Generic;
    using System.Linq;

    public abstract class LightBoxController : ILightBoxController
    {
        private readonly Int32 _capacity;
        private readonly ICommandInvoker _commandInvoker;
        private ICollection<LightBase> _lights;

        protected Int32 Capacity
        {
            get
            {
                return this._capacity;
            }
        }
        Int32 ILightBoxController.Capacity
        {
            get
            {
                return this.Capacity;
            }
        }
        protected ICommandInvoker CommandInvoker
        {
            get
            {
                return this._commandInvoker;
            }
        }
        protected ICollection<LightBase> Lights
        {
            get
            {
                return this._lights ?? (this._lights = new List<LightBase>());
            }
        }
        IEnumerable<LightBase> ILightContainer.Lights
        {
            get
            {
                return this.Lights;
            }
        }

        public LightBoxController(Int32 capacity, ICommandInvoker commandInvoker)
        {
            if (commandInvoker == null) throw new ArgumentNullException("commandInvoker");

            this._capacity = capacity;
            this._commandInvoker = commandInvoker;            
        }

        protected LightBase GetLightByID(String id)
        {
            if (String.IsNullOrEmpty(id) || String.IsNullOrWhiteSpace(id))
            {
                if (id == null) throw new ArgumentNullException("id");

                throw new ArgumentException("Can not be null or white space", "id");
            }

            LightBase light = this.Lights.SingleOrDefault(l => String.Compare(id, l.ID, StringComparison.OrdinalIgnoreCase) == 0);

            if (light == null) throw new LightNotFoundException("No light exists with the given ID within the controller");

            return light;
        }

        protected virtual void AddLight(LightBase light)
        {
            Console.WriteLine(
                "Adding light:{0}{1}",
                Environment.NewLine,
                light.ToString()
            ); 

            this.Lights.Add(light);
        }
        protected abstract void SwitchLightOnByID(String id);
        protected abstract void SwitchLightOffByID(String id);

        protected virtual void SwitchOnAllLights()
        {
            Console.WriteLine("Switching on all lights");

            foreach (LightBase light in this.Lights)
            {
                this.SwitchLightOnByID(light.ID);
            }
        }
        protected virtual void SwitchOffAllLights()
        {
            Console.WriteLine("Switching off all lights");

            foreach (LightBase light in this.Lights)
            {
                Console.ForegroundColor = light.Colour;

                this.SwitchLightOffByID(light.ID);

                Console.ForegroundColor = ConsoleColor.White;
            }
        }
        protected virtual void ToggleLights()
        {
            Console.WriteLine("Toggling all lights");

            foreach (LightBase light in this.Lights)
            {
                if (light.Status == LightStatus.On)
                {
                    Console.ForegroundColor = light.Colour;

                    light.SwitchOff();

                    Console.ForegroundColor = ConsoleColor.White;
                }
                else
                {
                    Console.ForegroundColor = light.Colour;

                    light.SwitchOn();

                    Console.ForegroundColor = ConsoleColor.White;
                }
            }
        }

        void ILightBoxController.AddLight(LightBase light)
        {
            this.AddLight(light);
        }
        void ILightBoxController.SwitchLightOnByID(String id)
        {
            this.SwitchLightOnByID(id);
        }
        void ILightBoxController.SwitchLightOffByID(String id)
        {
            this.SwitchLightOffByID(id);
        }
        void ILightBoxController.SwitchOnAllLights()
        {
            this.SwitchOnAllLights();
        }
        void ILightBoxController.SwitchOffAllLights()
        {
            this.SwitchOffAllLights();
        }
        void ILightBoxController.ToggleLights()
        {
            this.ToggleLights();
        }
    }
}
