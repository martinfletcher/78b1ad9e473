﻿namespace LightBoxChallenge.Controllers
{
    using LightBoxChallenge.Commands;
    using LightBoxChallenge.Domain;

    using System;

    public class MaxTenLightBoxController : DefaultLightBoxController
    {
        public MaxTenLightBoxController(Int32 capacity, ICommandInvoker commandInvoker) : base(capacity, commandInvoker)
        {
            if (capacity > 10) throw new InvalidOperationException("This controller can only store up to 10 lights");
        }

        protected override void AddLight(LightBase light)
        {
            if (this.Lights.Count == this.Capacity)
            {
                String message = String.Format(
                    "Light capacity reached ({0})",
                    this.Capacity
                );

                throw new InvalidOperationException(message);
            }

            base.AddLight(light);
        }
    }
}
