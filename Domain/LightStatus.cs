﻿namespace LightBoxChallenge.Domain
{
    using System;

    [Flags]
    public enum LightStatus
    {
        Off = 0,
        On = 1,
        //Dimmed = 2,
        //Strobe = 4
    }
}
