﻿namespace LightBoxChallenge.Commands
{
    using System;

    public interface ICommand
    {
        void Execute();
        void Undo();
    }
}
