﻿namespace LightBoxChallenge.Commands
{
    using LightBoxChallenge.Domain;

    using System;

    public class SwitchOffLightCommand : LightCommand
    {
        public SwitchOffLightCommand(LightBase light) : base(light)
        { }

        protected override void Execute()
        {
            this.Light.SwitchOff();
        }
        protected override void Undo()
        {
            this.Light.SwitchOn();
        }
    }
}
