﻿namespace LightBoxChallenge.Commands
{
    using LightBoxChallenge.Domain;

    using System;

    public class SwitchOnLightCommand : LightCommand
    {
        public SwitchOnLightCommand(LightBase light) : base(light)
        { }

        protected override void Execute()
        {
            this.Light.SwitchOn();
        }
        protected override void Undo()
        {
            this.Light.SwitchOff();
        }
    }
}
