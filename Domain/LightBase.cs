﻿namespace LightBoxChallenge.Domain
{
    using System;

    public abstract class LightBase
    {
        private readonly String _id;
        private readonly ConsoleColor _colour;

        public String ID
        {
            get
            {
                return this._id;
            }
        }
        public ConsoleColor Colour
        {
            get
            {
                return this._colour;
            }
        }
        public LightStatus Status { get; private set; }
        
        public LightBase(String id, ConsoleColor colour) : this(id, colour, LightStatus.Off)
        { }
        public LightBase(String id, ConsoleColor colour, LightStatus status)
        {
            if (String.IsNullOrEmpty(id) || String.IsNullOrWhiteSpace(id))
            {
                if (id == null) throw new ArgumentNullException("id");

                throw new ArgumentException("Can not be null or white space");
            }

            this._id = id;
            this._colour = colour;
            this.Status = status;
        }

        public virtual void SwitchOn()
        {
            Console.WriteLine("Switching Light On");

            this.Status = LightStatus.On;

            Console.WriteLine(this.ToString());
        }
        public virtual void SwitchOff()
        {
            Console.WriteLine("Switching Light Off");

            this.Status = LightStatus.Off;

            Console.WriteLine(this.ToString());        
        }

        public override string ToString()
        {
            return String.Format(
                "\tType: {1}, ID: {2}, Colour: {3}, Status: {4}{0}",
                Environment.NewLine,                
                this.GetType().Name,
                this.ID,
                this.Colour,
                this.Status
            );
        }
    }
}
