﻿namespace LightBoxChallenge.Commands
{
    using LightBoxChallenge.Domain;

    using System;

    public abstract class LightCommand : Command
    {
        private readonly LightBase _light;

        protected LightBase Light
        {
            get
            {
                return this._light;
            }
        }

        public LightCommand(LightBase light)
        {
            if (light == null) throw new ArgumentNullException("light");

            this._light = light;
        }
    }
}
