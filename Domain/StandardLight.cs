﻿namespace LightBoxChallenge.Domain
{
    using System;

    public class StandardLight : LightBase
    {
        public StandardLight(String id, ConsoleColor colour) : base(id, colour)
        { }
        public StandardLight(String id, ConsoleColor colour, LightStatus status) : base(id, colour, status)
        { }
    }
}
