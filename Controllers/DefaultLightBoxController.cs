﻿namespace LightBoxChallenge.Controllers
{
    using LightBoxChallenge.Commands;
    using LightBoxChallenge.Domain;

    using System;
    using System.Linq;

    public class DefaultLightBoxController : LightBoxController
    {
        public DefaultLightBoxController(Int32 capacity, ICommandInvoker commandInvoker) : base(capacity, commandInvoker)
        { }

        protected override void AddLight(LightBase light)
        {
            if (light == null) throw new ArgumentNullException("light");

            if (this.Lights.Any(l => l.ID == light.ID)) throw new InvalidCastException("A light already exists with the same ID");

            base.AddLight(light);
        }

        protected override void SwitchLightOnByID(String id)
        {
            LightBase light = this.GetLightByID(id);
            
            SwitchOnLightCommand command = new SwitchOnLightCommand(light);

            this.CommandInvoker.SetCommand(command);

            Console.ForegroundColor = light.Colour;

            this.CommandInvoker.Invoke();

            Console.ForegroundColor = ConsoleColor.White;
        }
        protected override void SwitchLightOffByID(String id)
        {
            LightBase light = this.GetLightByID(id);

            SwitchOffLightCommand command = new SwitchOffLightCommand(light);

            this.CommandInvoker.SetCommand(command);

            Console.ForegroundColor = light.Colour;

            this.CommandInvoker.Invoke();

            Console.ForegroundColor = ConsoleColor.White;
        }
        
    }
}
